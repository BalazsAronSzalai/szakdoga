// Imports
const express = require('express')
const expressLayouts = require('express-ejs-layouts')

const app = express()
const port = 3000


// Static Files
app.use(express.static('public'))
app.use('/css', express.static(__dirname + 'public/css'))
app.use('/js', express.static(__dirname + 'public/js'))
app.use('/img', express.static(__dirname + 'public/img'))

app.use(expressLayouts)
app.set('layout', './layouts/full-width')

/*app.get('', (req, res)=> {
    res.sendFile(__dirname + '/views/index.html')
})

app.get('/about', (req, res)=> {
    res.sendFile(__dirname + './views/about.html')
})*/

//Set Views
app.set('views', './views')
app.set('view engine', 'ejs')

app.get('', (req, res) => {
    res.render('home')
})

app.get('/contact', (req, res) => {
   // const index = path.join(__dirname, '/', 'views/about.ejs')
    res.render('contact')
})

app.get('/course', (req, res) => {
    // const index = path.join(__dirname, '/', 'views/about.ejs')
     res.render('course')
 })

 app.get('/price', (req, res) => {
    // const index = path.join(__dirname, '/', 'views/about.ejs')
     res.render('price')
 })

 app.get('/review', (req, res) => {
    // const index = path.join(__dirname, '/', 'views/about.ejs')
     res.render('review')
 })

 app.get('/teacher', (req, res) => {
    // const index = path.join(__dirname, '/', 'views/about.ejs')
     res.render('teacher')
 })

// Listen on port
app.listen(port, () => console.info(`Listening on port ${port}`))